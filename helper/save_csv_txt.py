import numpy as np
import json

def save_file_csv_or_txt(file_name,complete_report):
    ''' file_name: xyz.txt or xyz.csv
        complete_report: values to be saved  should be in array format
        complete_report = np.asarray([val1,val2,val3])'''
    np.savetxt(file_name,complete_report,delimiter=',',newline='')
    return True

def save_json(file_name,data,data_complete):
    data_list = list(data)
    complete_report = {"data_raw": str(data_list), "data_final_bits": str(data_complete)}
    #
    with open(file_name, 'w') as outfile:
        json.dump(complete_report, outfile, sort_keys=True, indent=4, ensure_ascii=False)

    return True
