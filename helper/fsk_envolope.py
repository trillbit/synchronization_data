import numpy as np
from helper.BPF_comp_lfilter_final import bpf_lfilter as bpf_lfilter
from helper.LPF_FILTFILT_final import Lpf_filtfilt as Lpf_filtfilt
import scipy.signal as signal
import matplotlib.pyplot as plt

def fsk_demod_envelop(received_signal, Fs, Fc, frames):
    # bpf for path 1 with frequency fc+del_f
    n=1
    # Fbit = float(Fs) / float(frames)
    Fbit = float(Fs) / 40
    print "Fbit",Fbit,int(Fbit*n)
    #    BAND PASS FILTERING fc+del_f
    lower_cutoff = 16000
    higher_cutoff = 19000
    transition_width = 100
    bpf_filtered_plus_del_f = bpf_lfilter(received_signal, lower_cutoff, higher_cutoff, Fs, transition_width)

    # HILBERT TRANSFORM
    received_hilbert_plus_del_f = np.diff(bpf_filtered_plus_del_f, 1)
    received_envelop_plus_del_f = np.abs(signal.hilbert(received_hilbert_plus_del_f))
    received_filtered_plus_del_f = Lpf_filtfilt(received_envelop_plus_del_f, int(Fbit*n), Fs)





    #    print 'length of lpf signal',len(received_filtered_plus_del_f)
    # bpf for path 1 with frequency fc-del_f
    #    BAND PASS FILTERING fc-del_f
    # lower_cutoff = 18500
    # higher_cutoff = 19500
    # transition_width = 200
    # bpf_filtered_minu_del_f = bpf_lfilter(received_signal, lower_cutoff, higher_cutoff, Fs, transition_width)
    #
    # # HILBERT TRANSFORM
    # received_hilbert_minu_del_f = np.diff(bpf_filtered_minu_del_f, 1)
    # received_envelop_minu_del_f = np.abs(signal.hilbert(received_hilbert_minu_del_f))
    # received_filtered_minu_del_f = Lpf_filtfilt(received_envelop_minu_del_f, int(Fbit * 3), Fs)
    #    print 'length of lpf signal',len(received_filtered_minu_del_f)
    plt.figure()
    plt.plot(received_signal)
    plt.hold(True)
    plt.plot(received_filtered_plus_del_f)
    plt.show()
    plt.figure()
    plt.plot(received_envelop_plus_del_f)
    plt.hold(True)
    plt.plot(received_filtered_plus_del_f, 'r')
    plt.show()

    # down_sampled_output = []
    # data_sz = len(received_signal)/frames
    # # comparision of path 1 to path 2 data for desition making
    # for i in range(0, data_sz):
    #     #        print [i*frames,i*frames+frames]
    #     path_1 = np.mean(received_filtered_plus_del_f[i * frames:i * frames + frames])
    #     #        print 'mean of each bit in path 1 is %d i, %f path_1'%(i,path_1)
    #     path_2 = np.mean(received_filtered_minu_del_f[i * frames:i * frames + frames])
    #     #        print 'mean of each bit in path 2 is %d i, %f path_2'%(i,path_2)
    #     if path_1 > path_2:
    #         down_sampled_output.append(1)
    #     else:
    #         down_sampled_output.append(-1)

    # return down_sampled_output
    return True