## RLS Equlaizer for training and data
# INTRODUCTION : 1. vdot--> C++ dot;
#                2. np.multiply=normal multiply [complex vec]*[complex vector] [(-2.70915170341+2.79509516159j)*(0.99999939500006096+0.0010999997781666801j)=answer:(-2.7122246684310882+2.7921134042848266j)]



import numpy as np
#from dec4psk import dec4psk
import matplotlib.pyplot as plt
import cmath

def dec4psk(signal_decision):		# dtype = cvec, signal for decision  
    xr = np.real(signal_decision)
    xi = np.imag(signal_decision)
    dr = np.sign(xr)
    di = np.sign(xi)
    decision = np.complex(dr ,di)
    # decision = d / np.sqrt(2)
    return decision		       # dtype=cvec signal after decision

def RLS_Equlalizer(signal_rls_equalizer,no_training,desired_bits_training,Nd,N_FF_taps,M_FB_taps,delta,L_lamda,Ns,Nplus,FS,Kf1,Kf2,Algo):

    """ Ns= point selection type= int, taken 2
            delta=0.001
            L_lamda= lamda=0.998
            desired_bits_training= known sequence for training
            Nplus= number of future samples=4
            FS=2

        """

    #initialization
    # print signal_rls_equalizer,len(signal_rls_equalizer)


        # raw_input()


    phase_updated=0                                                                 # DTYPE:FLOAT   f(1)=0
    a=np.zeros(N_FF_taps)                                                           # DTYPE:CVEC ,Feedforward equlaizer taps
    b=np.zeros(M_FB_taps)                                                           # DTYPE:CVEC ,Feedback equalizer taps
    c=np.hstack((a,b))                                                              # DTYPE:CVEC ,vector hold updated FF and FB tap values
    x=np.zeros(N_FF_taps)                                                           # DTYPE:CVEC  ,input vector for Feedforward section
    Sf=0                                                                            # DTYPE:FLOAT ,PLL integrator
    dd=np.zeros(M_FB_taps)                                                          # DTYPE:CVEC  ,previous decision vector
    sse=0                                                                           # DTYPE:FLOAT ,sum square error=0 initially
    P=np.eye(N_FF_taps+M_FB_taps)/delta                                             # DTYPE=matrix [50x50], 50=N_FF_taps+M_FB_taps,p_iterative vetor for kalman gain calculation
    Lf=1
    # Nd=2000
    de_vec=[]                                                                       # DTYPE:CVEC
    error_vec=[]                                                                    # DTYPE:CVEC
    desired_bit_vec=[]                                                              # DTYPE:CVEC
    training_vec=[]
    phase_updated_vec=[]                                                            # DTYPE:CVEC
    error_square_vec=[]                                                             # DTYPE:CVEC
    delay_comp=np.zeros(int(np.ceil(N_FF_taps/2)))                                  # dtype int, half Feedforward tap values
    signal_rls_equalizer_comp=np.concatenate([delay_comp,signal_rls_equalizer])     # half tap values delay introduces if not compensated results error in last bits


    for i in range (0,Nd):
        # print i
        nb=(i*Ns)+(Nplus-1)*Ns                                                          # number of bit to be selected
        xn= signal_rls_equalizer_comp[nb+int(np.ceil((Ns/FS)/2)):nb+Ns:int(Ns/FS)]
        x = np.append(xn,x)
        x=x[:N_FF_taps]

        # phase correction feedforward  section
        p=np.multiply(np.vdot(np.transpose(a),x),np.exp(-1j*phase_updated))
        # print 'p',p,np.vdot(np.transpose(a),x)

        ## feedback loop
        q=np.vdot(np.transpose(b),dd)
        de= p-q
        de_vec=np.append(de_vec,de)

        if Algo=="bpsk":
            if i>=no_training:
                desired_bits_data = np.sign(np.real(de))           ## BPSK
                desired_bit=desired_bits_data
            else:
                bits_training = np.sign(np.real(de))                ## BPSK
                desired_bit=desired_bits_training[i]
        else:
            if i>=no_training:
                desired_bits_data=dec4psk(de)
                desired_bit=desired_bits_data
            else:
                bits_training = dec4psk(de)                           ## BPSK
                desired_bit=desired_bits_training[i]


        desired_bit_vec=np.append(desired_bit_vec,desired_bit)
        training_vec=np.append(training_vec,bits_training)




        error=desired_bit-de                                                     # error estimate
        error_vec=np.append(error_vec,error)
        error_square=np.abs(error**2)                                            # error
        error_square_vec=np.append(error_square_vec,error_square)
        sse=sse+error_square                                                     # sum of error square

        mse=sse/(i+1)                                                            # mean square error of sse index starts from 0 in python

        ## update filter parameters

        y=np.append(np.dot(x,np.exp(-1j*phase_updated)), dd)
        y=np.reshape(y,(1,len(y)))

        Sf=Lf*Sf+np.imag(p*np.conjugate(desired_bit+q))                                          # PLL integrator updation
        phase_updated=phase_updated+Kf1*np.imag(p*np.conjugate(desired_bit+q))+Kf2*Sf            # phase updation
        phase_updated_vec = np.append(phase_updated_vec, phase_updated)
        # print "phase", phase_updated

        # calculate kalman gain
        k=np.dot((P/L_lamda), np.transpose(y))/(1+(np.dot(np.dot(np.conjugate(y),(P/L_lamda)),np.transpose(y))))
        c=c+np.transpose(k)*np.conjugate(error)
        P =(P / L_lamda) -  np.dot(k * np.conjugate(y),( P / L_lamda))
        a = c[0][:N_FF_taps]
        b = -c[0][N_FF_taps :N_FF_taps + M_FB_taps]

        dd = np.append(desired_bit,dd)
        dd = dd[0:M_FB_taps]
        # print "\n"



    return desired_bit_vec,training_vec,de_vec,error_square_vec,phase_updated_vec







