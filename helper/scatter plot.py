import numpy as np
import matplotlib.pyplot as plt


import json
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from collections import OrderedDict
import numpy as np

FileConfig = {

            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/TEST10KBITS.wav":"unrecorded",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_26.wav": "0-1 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_27.wav": "0-2 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_28.wav": "0-3 meter",

            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_29.wav": "1-1 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_30.wav": "1-2 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_31.wav": "1-3 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_32.wav": "1-4 meter",

            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_33.wav": "2-1 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_34.wav": "2-2 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_35.wav": "2-3 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_36.wav": "2-4 meter",

            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_37.wav": "6-1 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_39.wav": "6-2 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_40.wav": "6-3 meter",

            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_41.wav": "8-1 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_42.wav": "8-2 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_43.wav": "8-3 meter",
            "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_44.wav": "8-4 meter"

            #
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/TEST10KBITS.wav": "org",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_26.wav": "0 -1 Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_27.wav": "0 - 2 Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_28.wav": "0 - 3 Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_29.wav": "1 - 1 Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_30.wav": "1 - 2 Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_31.wav": "1 - 3 Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_32.wav": "1 - 4 Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_42.wav": "8 - 1  Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_43.wav": "8 - 2 Meters",
            # "/home/roopa/baseband_corr_zerocrossing_peak/waves_10kbits/default_44.wav": "8 - 3 Meters"

}

def read_from_file(filename):
    return json.load(open(filename))

def createPrimaryKey(key, data, ref_key):
    complete_result = {}
    for element in data:
        result = {}
        for sub in data[element]:
            if not str(sub[key]) in result:
                result[str(sub[key])] = []
            result[str(sub[key])].append(sub[ref_key])
        complete_result[element] = result
    return complete_result

def plot_data(y, filename, taps):
    print taps
    x = [10, 50, 100, 500, 1000, 1500, 2500, 5000, 8000, 10000]
    plt.title(FileConfig[filename] + " || " + str(taps))
    plt.scatter(x,y)

    plt.xlim([10,10000])
    plt.xlabel("delta")
    plt.ylabel("error percentage %")
    # plt.show()


def surf_plot(avg_err, tap, dl, distance):
    # print tap
    # print avg_err
    # print dl

    # raw_input()
    # dl = [10, 50, 100, 500, 1000, 1500, 2500, 5000, 8000, 10000]
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # fig = Axes3D.figure()
    # surf = ax.plot_surface(dl, avg_err, tap, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    print len(dl), len(avg_err), len(tap)
    ax = Axes3D(plt.gcf())
    ax.set_xlabel('delta value')
    ax.set_xlim([10, 10000])
    ax.set_ylabel('average error %')
    ax.set_ylim([0, 100])
    ax.set_zlabel('taps')
    cm = plt.get_cmap("RdYlGn")
    col = np.arange(80)
    ax.scatter(dl[:80], avg_err[:80], zs=tap[:80], s=20, c=col, depthshade=True, cmap=cm)
    # ax.plot(dl, avg_err, tap, label='parametric curve')
    # fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.title('plot of delta vrs avg error for tap value  for distance % s' % distance)
    plt.show()

def scatter_plot(avg_err,tap,dl,distance):
    print tap
    print avg_err
    print dl


    # raw_input()
    # dl = [10, 50, 100, 500, 1000, 1500, 2500, 5000, 8000, 10000]
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # fig = Axes3D.figure()
    # surf = ax.plot_surface(dl, avg_err, tap, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    print len(dl), len(avg_err), len(tap)
    ax = Axes3D(plt.gcf())
    ax.set_xlabel('delta value')
    ax.set_xlim([10,10000])
    ax.set_ylabel('average error %')
    ax.set_ylim([0,100])
    ax.set_zlabel('taps')
    cm = plt.get_cmap("RdYlGn")
    col = np.arange(len(dl))
    ax.scatter(dl, avg_err, zs=tap, s=20, c=col, depthshade=True, cmap=cm)
    # ax.plot(dl, avg_err, tap, label='parametric curve')
    # fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.title('plot of delta vrs avg error for tap value  for distance % s'% distance)
    plt.show()




if __name__ == '__main__':
    filename = "/home/roopa/qpsk_milica/json_csv/60_ov_fact.json"
    data = read_from_file(filename)# json file red

    for file in sorted(data):
        select = data[file]
        ff_taps,fb_taps,data_error = [], [],[]
        # error_per = []
        for element in select:
            ff_taps.append(element["FF_taps"])
            fb_taps.append(element["FB_taps"])
            data_error.append(element["data error"])


        # print FileConfig[file]
        print "ff_taps=",ff_taps
        print "fb_taps=",fb_taps
        print "data_error=",data_error
        print "\n"

        cmap = cm.get_cmap(name='rainbow')
        #
        k = 0
        for (x, y) in zip(ff_taps, fb_taps):
            plt.text(x, y + 0.01, "{}".format(data_error[k]))
            k += 1
            plt.figure(1)
            plt.scatter(ff_taps, fb_taps, color=cmap(x, y))
            plt.grid(True)

        # plt.xlim([0, 1])
        # plt.ylim([2, 18])
        plt.xlabel('ff taps')
        plt.ylabel('fb taps')
        # plt.title("plot of avg error vrs taps  for different training at distance unrecorded")
        plt.show()

##################################
from scipy.io.wavfile import read, write
import numpy as np
import scipy.signal as signal
from src.common.libraries.filternew import bpf_filtfilt
import matplotlib.pyplot as plt
from screenshot import get_Screenshot
from src.common.helpers.plot_fft import plot_fft


#### read signal

data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_40ov_1000bit_data_test.wav")[1] # unrecorded

#
# # 0 meter
# data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/0mtr_1.wav")[1]  # Transmitted symbols
# data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/0mtr_2.wav")[1]  # Transmitted symbols
# data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/0mtr_3.wav")[1]  # Transmitted symbols
# plt.plot(data)
# plt.show()
#
# # 1 mtr
#
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/1mtr_1.wav")[1]  # Transmitted symbols
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/1mtr_2.wav")[1]  # Transmitted symbols
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/1mtr_3.wav")[1]  # Transmitted symbols
#
#
# # 2 mtr
#
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/2mtr_1.wav")[1]  # Transmitted symbols
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/2mtr_2.wav")[1]  # Transmitted symbols
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/2mtr_3.wav")[1]  # Transmitted symbols
#
#
# #  4 mtr
#
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/4mtr_1.wav")[1]  # Transmitted symbols
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/4mtr_2.wav")[1]  # Transmitted symbols
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/4mtr_3.wav")[1]  # Transmitted symbols
#
#
# # 6 mtr
#
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/6mtr_1.wav")[1]  # Transmitted symbols
# data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/6mtr_2.wav")[1]  # Transmitted symbols
# # data = read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/test_3_waves/6mtr_3.wav")[1]  # Transmitted symbols
#
#
sync_signal=read("/home/roopa/bpsk_logic_upfirdn/upfirdn_waves/sync_upfirdn_81frames_17.6K_amp_0.75_rrc.wav")[1]
corr_op = np.correlate(data, sync_signal, 'same')
plt.plot(corr_op)
plt.show()

# # plt.title("correlation received signal")
# # plt.xlim([0,30000])
# # plt.ylim([-1.5,1.5])
#
#
#
# ## sepearate sync and data
# filtered_sync = bpf_filtfilt(signal_to_filter=data, lower_cutoff=17500, higher_cutoff=18000,
#                              sampling_rate=44100, transition_width=105, ripple_db=120)
# # plot_fft(filtered_sync,44100)
#
# filtered_data = bpf_filtfilt(signal_to_filter=data, lower_cutoff=18500, higher_cutoff=19500,
#                              sampling_rate=44100, transition_width=105, ripple_db=120)
#
# # plot_fft(filtered_data,44100)
# ## perform welch transform to analyze power
# f_sync, psd_sync = signal.welch(filtered_sync, 44100)
f_data, psd_data = signal.welch(filtered_data, 44100)
plt.plot(f_data,10 * np.log10(psd_data),label="max power dat = %d dB"%max(10 * np.log10(psd_data)))
# plt.plot(f_sync,10 * np.log10(psd_sync),label="max power sync = %d dB"%max(10 * np.log10(psd_sync)))
#
#
# plt.legend(loc='best')
# plt.xlabel("freq")
# plt.ylabel("psd")
# plt.title("power spectrum using welch after AGC %d in dB"%max(10 * np.log10(psd_sync)))
# plt.title("power spectrum using welch after AGC %d in dB"%max(10 * np.log10(psd_data)))


# sync_power_1=[-70,-81,-81,-81,-83]# 0 to 6 meter 1 set
# data_power_1=[-59,-67,-70,-69,-70]
#
# sync_power_2=[-72,-81,-81,-81,-81]# 0 to 6 meter 2nd set
# data_power_2=[-60,-68,-70,-69,-69]
#
# sync_power_3=[-72,-81,-81,-81,-81]# 0 to 6 meter 23rd set
# data_power_3=[-60,-68,-70,-69,-69]


sync_power_1=[-81,-81,-83]# 0 to 6 meter 1 set
data_power_1=[-70,-69,-70]

sync_power_2=[-81,-81,-81]# 0 to 6 meter 2nd set
data_power_2=[-70,-69,-69]

sync_power_3=[-81,-81,-81]# 0 to 6 meter 23rd set
data_power_3=[-70,-69,-69]
minimum_mu=[0.0025,0.0045,0.0049]
# cm = plt.cm.get_cmap('RdYlBu')

plt.figure(1)
plt.subplot(1,2,1)
i=0
count=['0 mtr','1 mtr','2 mtr','4 mtr','6 mtr']
for (x, y) in zip(sync_power_1,minimum_mu):
    plt.text(x, y, "{}".format(count[i]))
    i += 1

plt.plot(sync_power_1,minimum_mu)
plt.ylim([0.001,0.005])
plt.xlim([-68,-90])
plt.xlabel("power")
plt.ylabel("minimum mu")
plt.title('sync power vrs minimum mu plot')
plt.subplot(1,2,2)
i=0
count=['0 mtr','1 mtr','2 mtr','4 mtr','6 mtr']
for (x, y) in zip(data_power_1,minimum_mu):
    plt.text(x, y, "{}".format(count[i]))
    i += 1
plt.plot(data_power_1,minimum_mu)
plt.ylim([0.001,0.005])
plt.xlim([-55,-75])
plt.xlabel("power")
plt.ylabel("minimum mu")
plt.title('data power vrs minimum mu plot')
plt.legend(loc='best')
get_Screenshot("1st set of signal")


plt.figure(2)
plt.subplot(1,2,1)
i=0
count=['0 mtr','1 mtr','2 mtr','4 mtr','6 mtr']
for (x, y) in zip(sync_power_2,minimum_mu):
    plt.text(x, y, "{}".format(count[i]))
    i += 1

plt.plot(sync_power_2,minimum_mu)
plt.ylim([0.001,0.005])
plt.xlim([-68,-90])
plt.xlabel("power")
plt.ylabel("minimum mu")
plt.title('sync power vrs minimum mu plot')
plt.subplot(1,2,2)
i=0
count=['0 mtr','1 mtr','2 mtr','4 mtr','6 mtr']
for (x, y) in zip(data_power_2,minimum_mu):
    plt.text(x, y, "{}".format(count[i]))
    i += 1
plt.plot(data_power_2,minimum_mu)
plt.ylim([0.001,0.005])
plt.xlim([-55,-75])
plt.xlabel("power")
plt.ylabel("minimum mu")
plt.title('data power vrs minimum mu plot')
plt.legend(loc='best')
get_Screenshot("2nd set of signal")

plt.figure(3)
plt.subplot(1,2,1)
i=0
count=['0 mtr','1 mtr','2 mtr','4 mtr','6 mtr']
for (x, y) in zip(sync_power_3,minimum_mu):
    plt.text(x, y, "{}".format(count[i]))
    i += 1

plt.plot(sync_power_3,minimum_mu)
plt.ylim([0.001,0.005])
plt.xlim([-68,-90])
plt.xlabel("power")
plt.ylabel("minimum mu")
plt.title('sync power vrs minimum mu plot')
plt.subplot(1,2,2)
i=0
count=['0 mtr','1 mtr','2 mtr','4 mtr','6 mtr']
for (x, y) in zip(data_power_3,minimum_mu):
    plt.text(x, y, "{}".format(count[i]))
    i += 1
plt.plot(data_power_3,minimum_mu)
plt.ylim([0.001,0.005])
plt.xlim([-55,-75])
plt.xlabel("power")
plt.ylabel("minimum mu")
plt.title('data power vrs minimum mu plot')
plt.legend(loc='best')
# get_Screenshot("3rd set of signal")

# get_Screenshot("1st set of signal")
# plt.show()
