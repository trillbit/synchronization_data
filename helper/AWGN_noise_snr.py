'''Introduction:generate AWGN: Additive white gaussain noise signal for required SNR inn dB

Author: Richard Lyons Page number=764,765 '''



import numpy as np

import matplotlib.pyplot as plt



def SNR_set_AWGN(signal_noisefree, Desired_snr_dB):

    '''k : noise level scaling factor calculated  wrt to desired snr dB'''

    Npt= len(signal_noisefree)

    # generate awgn 0 mean unit variance of size of signal

    Noise= np.random.normal(loc=0.0, scale=1.0,size=Npt)          # awgn generated

    Signal_power=np.sum(np.abs(signal_noisefree)**2)/Npt          # calculate signal power

    Noise_power=np.sum(np.abs(Noise)**2)/Npt                                # calculate noise power


    k=(Signal_power/Noise_power)*(10**(-Desired_snr_dB/10.0))                           # amplitude 20 log10(As/An), power = 10 log10(Ps/Pn)

    New_Noise = np.multiply(np.sqrt(k) ,Noise)

    New_Noise_Power = np.sum(New_Noise**2)/Npt


    Noisy_Signal= signal_noisefree+New_Noise

    New_SNR = 10 * np.log10(Signal_power / New_Noise_Power)
    obtained_SNR = 10 * np.log10(Signal_power / Noise_power)
    print "Actual snr", obtained_SNR, "signal power=",Signal_power, "Noise power=", New_Noise_Power,"new_snr",New_SNR ,"\n\n"

    return Noisy_Signal