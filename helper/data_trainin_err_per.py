import json
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
from helper.screenshot import get_Screenshot
compiled_data_set = json.load(open('/home/roopa/qpsk_milica/error signal room.json'))



training_len_defined = [100,200,300,400]


def plotPicture(data_err_per,training_err_per,counter):
    '''
    Plotting data for all 5 distance
    :param response:
    :return:
    '''
    handles = []
    colors = ["#FF0000", "#2DC2F2", "#008000", "#9400D3"]
    # counter = 0

    # area = np.pi * 3

    area = (10-(counter*2)) ** 2  # 0 to 15 point radii
    for resp in range (0,len(data_err_per)):
        plt.scatter(training_err_per[resp], data_err_per[resp],s=area, color=colors[counter],alpha=0.5)
        plt.xlim([-1,50])
        plt.ylim([0,50])
        plt.xlabel("training_err_per")
        plt.ylabel("data_err_per")


    for color, length in zip(colors, training_len_defined):
        handles.append(mpatches.Patch(color=color, label=length))
    plt.legend(handles=handles, loc='upper left', ncol=5)
    plt.hold(True)

def plotData():
    '''
    Collected data from json, arrange and plot
    :return: null
    '''
    training_len = []
    training_err_per_100 = []
    training_err_per_200 = []
    training_err_per_300 = []
    training_err_per_400 = []
    data_error_per_100 = []
    data_error_per_200 = []
    data_error_per_300 = []
    data_error_per_400 = []
    for element in compiled_data_set:
        training_len=(float(element["training length"]))
        if training_len==100:
            training_err_per_100.append(float(element["training error per"]))
            data_error_per_100.append(float(element["data error perc"]))
        elif training_len==200:
            training_err_per_200.append(float(element["training error per"]))
            data_error_per_200.append(float(element["data error perc"]))
        elif training_len==300:
            training_err_per_300.append(float(element["training error per"]))
            data_error_per_300.append(float(element["data error perc"]))
        elif training_len == 400:
            training_err_per_400.append(float(element["training error per"]))
            data_error_per_400.append(float(element["data error perc"]))
    print training_err_per_100,data_error_per_100
    plotPicture(training_err_per_100,data_error_per_100,counter=0)
    plotPicture(training_err_per_200,data_error_per_200,counter=1)
    plotPicture(training_err_per_300,data_error_per_300,counter=2)
    plotPicture(training_err_per_400,data_error_per_400,counter=3)
    plt.title("plot of training error perc vrs data error perc channel room")
    # plt.show()
    get_Screenshot("plot of training error perc vrs data error perc channel room")
    # get_Screenshot("plot of training error perc vrs data error perc channel dining-hall")
    # get_Screenshot("plot of training error perc vrs data error perc channel outdoor")



if __name__ == '__main__':
    plotData()
