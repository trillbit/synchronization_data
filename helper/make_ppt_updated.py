from pptx import Presentation
from pptx.util import Inches
import os
import numpy as np

class MakePPT():

    def __init__(self, **kwargs):
        self.folder = kwargs['folder']
        self.filename = kwargs['filename']
        self.prs = self.create_presentation()

    def create_presentation(self):
        prs = Presentation()
        return prs

    def add_slide_1(self):
        prs = self.prs
        blank_slide_layout = prs.slide_layouts[6]

        slide = prs.slides.add_slide(blank_slide_layout)
        left = Inches(1)
        top = Inches(.1)
        width = Inches(10)
        height = Inches(.5)
        txBox = slide.shapes.add_textbox(left, top, width, height)
        tf = txBox.text_frame
        tf.text = self.slide_1_image.split("/")[-1].replace("_"," ").replace(".png","").replace("mobile ", "Mobile Played : ").replace("speaker ", "Speaker Played : ")
        left = Inches(.1)
        top = Inches(.5)
        width = Inches(10)
        pic = slide.shapes.add_picture(self.slide_1_image, left, top, width=width)
        return True

    def create_ppt(self):
        """
        get all images from directory and upload it to ppt
        :return:
        """
        path, dirs, files = os.walk(self.folder).next()
        files = np.sort(files)
        for element in files:
            self.slide_1_image = self.folder + "/" + element
            self.add_slide_1()

        self.prs.save(self.filename)


if __name__ == '__main__':
    MakePPT(
        folder='/home/sid/work/python_code/python_analysis/plots',
        filename = '/home/sid/work/python_code/python_analysis/powerplot1024_2048.pptx'
    ).create_ppt()
