import os, json
import numpy as np
# from src.common.classes.create_filter import create_filter_length
import matplotlib.pyplot as plt

BASE_DIRCTORY   = os.path.dirname(os.path.abspath(__file__)).split("helper")[0]
configration    = os.path.join(BASE_DIRCTORY,'mainconfig_fsk.json')




class RRC():
    # print "rrc filter activated"
    def __init__(self, **kwargs):
        self.algo = kwargs["algo"]



        with open(configration) as data_file:
            config = json.load(data_file)['configrations'][self.algo]


        self.roll_off       = config["roll_off"]
        self.ov_fact        = config["ov_fact"]
        self.span           = config["span"]
        self.frames           = self.span*self.ov_fact

        print "RRC parameter length", self.algo, self.frames



    @property
    def get_rrc_factor(self):

        rrc_output = self.rrc(self.roll_off, self.span, self.ov_fact)

        return rrc_output


    def rrc(self,a,span,ov_fact):
        """
        :param a: roll off factor
        :param t: Filter Length
        :param N: No of bits
        :return:
        """

        step = 1 / float(ov_fact)
    ######### order of filter should be even
        if (span * ov_fact) % 2!=0.0 or  a >1.0:
            order= span * ov_fact
            raise Exception("filter order is not even or roll off not valid")

        delay = (span * ov_fact) / 2.0
        Ts = float(1) / float(44100)

        t = np.arange((-delay * step), (delay * step) + step, step)

        p = np.zeros_like(t)

        # implementation of Squareroot raisercoisne  for equation implementation : https://en.wikipedia.org/wiki/Root-raised-cosine_filter
        for i in range(0, len(t)):
            if t[i] == 0:
                p[i] = 1.0 / np.sqrt(Ts) * ((1 - a) + 4 * a / np.pi)
            elif t[i] == float(1) / float(4 * a) or t[i] == -float(1) / float(4 * a):
                p[i] = a / np.sqrt(2 * Ts) * ((1 + 2 / np.pi) * np.sin(np.pi / (4 * a)) + (1 - 2 / np.pi) * np.cos(np.pi / (4 * a)))
            else:
                # print "t val",t[i]
                Numerator = np.float(np.sin(np.pi * t[i] * (1 - a)) + 4 * a * t[i] * np.cos(np.pi * t[i] * (1 + a)))
                # print "numerator", Numerator
                dinominator =np.float( np.pi * t[i] * (1 - (4 * a * t[i]) ** 2))
                # print "dinominator", dinominator
                # if dinominator==0:
                #     dinominator=np.finfo(float).eps
                #     print "+++++++++++++++++++>>>>>>>>>>>" , 1 / np.sqrt(Ts) * Numerator / dinominator
                # else:
                p[i] = 1 / np.sqrt(Ts) * Numerator / dinominator

                # Normalization to unit energy rms
        rrcfilt = p / float(np.sqrt(np.sum(p ** 2)))

        return rrcfilt

if __name__ == "__main__":
    
    a=0.25
    Fs=44100.0
    ov_fact=4
    span=3
    h_rrc=RRC(roll_off=a ,ov_fact=ov_fact, span=span).get_rrc_factor
    print h_rrc
    plt.figure(2)
    plt.stem(h_rrc)
