# -*- coding: utf-8 -*-

"""
Created on Wed Feb 08 11:59:19 2017

@author: Roopa

References:
https://tomroelandts.com/articles/how-to-create-a-configurable-filter-using-a-kaiser-window
https://in.mathworks.com/help/signal/ug/compensate-for-delay-and-distortion-introduced-by-filters.html
https://in.mathworks.com/help/signal/ref/filtfilt.html

        for bpf_filtfilt

Input:  signal_to_filter=signal to be filtered
        lower_cutoff=lower cutoff frequency
        higher_cutoff=higher cutoff frequency
        sampling_rate=Fs=44100
        transition_width=205 Hz

output:  filtered_signal= signal obtained after bandpass filtering
         filter_coefficient= tap/filter coefficients values
         Number_of_coefficients= number of coefficiens

         for bpf_lfilter

Input:  signal_to_filter=signal to be filtered
        lower_cutoff=lower cutoff frequency
        higher_cutoff=higher cutoff frequency
        sampling_rate=Fs=44100
        transition_width=205 Hz

output:  filtered_signal= signal obtained after bandpass filtering
         filter_coefficient= tap/filter coefficients values
         Number_of_coefficients= number of coefficiens
         t_comp= time vector with delay samples removed from end
         originalsignal_comp=original sine wave before filtering truncated by removing delay samples from end
         filtered_withoutcomp=filtered signal  with delay samples removed from beginning


"""

import scipy.io.wavfile as sci
import numpy as np
import math
from scipy.signal import kaiserord, filtfilt, lfilter, firwin, freqz, group_delay

Fs = 44100.0

# # transition Bandwidth from stop band to pass band BPF
# transition_width = 205
# # cutoff freuency
# lower_cutoff = 17000
# higher_cutoff = 19000

# function for performing bandpass filtering using filtfilt
def bpf_filtfilt(signal_to_filter, lower_cutoff, higher_cutoff, sampling_rate, transition_width,ripple_db):
    # Nyquist rate is half of sampling rate
    nyq_rate = sampling_rate / 2.0

    # The desired width of the transition from pass to stop,
    # relative to the Nyquist rate transition width.
    width = transition_width / nyq_rate  # transition bandwidth 5Hz wrt nyquist rate

    # The desired attenuation in the stop band, in dB.
    # ripple_db = 60.0  # stop band attenuation

    # Design a Kaiser window to limit ripple and width of transition region
    N, beta = kaiserord(ripple_db, width)
    # print "BETA FROM KAISER BPF",beta
    # print "NUMBER OF TAPS FROM KAISER BPF", N

    # The cutoff frequency of the filter.
    lower_cutoff_hz = lower_cutoff
    high_cutoff_hz = higher_cutoff

    # Use firwin with a Kaiser window to create a band pass FIR filter.
    taps = firwin(N, [lower_cutoff_hz / nyq_rate, high_cutoff_hz / nyq_rate], window=('kaiser', beta), pass_zero=False)

    # DELAY CALCULATION
    w, gd = group_delay((taps, 1.0))
    delay = math.ceil(np.mean(gd))
    # zero phase digital filter using filtfilt command pad delay number of zeros at end of signal before filtering using padlen=int(delay)
    filtered_by_filtfilt = filtfilt(taps, 1.0, signal_to_filter, padlen=int(delay))
    return filtered_by_filtfilt


# function for performing bandpass filtering using lfilter
def bpf_lfilter(signal_to_filter, lower_cutoff, higher_cutoff, sampling_rate, transition_width):

    nsamples = len(signal_to_filter)
    t = np.arange(nsamples) / Fs

    # Nyquist rate is half of sampling rate
    nyq_rate = sampling_rate / 2.0

    # The desired width of the transition from pass to stop,
    # relative to the Nyquist rate transition width.
    width = transition_width / nyq_rate  # transition bandwidth 5Hz wrt nyquist rate

    # The desired attenuation in the stop band, in dB.
    ripple_db = 60.0  # stop band attenuation

    # Design a Kaiser window to limit ripple and width of transition region
    N, beta = kaiserord(ripple_db, width)

    # The cutoff frequency of the filter.
    lower_cutoff_hz = lower_cutoff
    high_cutoff_hz = higher_cutoff

    # Use firwin with a Kaiser window to create a band pass FIR filter.
    taps = firwin(N, [lower_cutoff_hz / nyq_rate, high_cutoff_hz / nyq_rate], window=('kaiser', beta), pass_zero=False)
    filtered_x = lfilter(taps, 1.0, signal_to_filter)

    # DELAY CALCULATION
    w, gd = group_delay((taps, 1.0))
    delay = math.ceil(np.mean(gd))

    # DELAY COMPENSATION
    t_comp = t[0:int(-delay)]
    originalsignal_comp = signal_to_filter[0:int(-delay)]

    # Shift the filtered signal by removing its first delay samples
    kaiser_bpf_signal_compensated = np.delete(filtered_x, np.s_[int(0):int(delay)], 0)

    return kaiser_bpf_signal_compensated, taps, N, t_comp, originalsignal_comp, filtered_x

# if __name__ == "__main__":
#     # function call for filtfilt
#     filtered_signal, filter_coefficient, Number_of_coefficients = bpf_filtfilt(signal_for_filtering, lower_cutoff,
#                                                                                higher_cutoff, Fs, transition_width)
#
#
#     filtered_signal, filter_coefficient, Number_of_coefficients, t_comp, originalsignal_comp, filtered_withoutcomp = bpf_lfilter(
#         signal_for_filtering, lower_cutoff, higher_cutoff, Fs, transition_width)

