
import scipy.signal as signal
import matplotlib.pyplot as plt
import numpy as np

def power(data,fs):
    ##periodogram using welch
    f, psd = signal.welch(data, fs,nperseg=256)
    max_power_db=10 * np.log10(np.max(psd))
    # plt.figure()
    # plt.plot(10 * np.log10(psd))
    # plt.xlabel("freq")
    # plt.ylabel("psd")
    # plt.title("power spectrum in dB using welch  %f peak power "%max_power_db)
    # plt.show()
    return f, psd,max_power_db
