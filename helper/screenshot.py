import pyscreenshot as ImageGrab
import numpy
import time
from matplotlib import pyplot as plt

def get_Screenshot(imagename):
    folder = "/home/roopa/FSK_Baseband/reference/screenshot/"
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.show(block=False)
    time.sleep(1)
    im = ImageGrab.grab(bbox=(100, 10, 1300, 725))  # (base, top,left , right)
    filename = folder+"{}.png".format(str(imagename))
    im.save(filename)
    im.close()
    time.sleep(1)
    print "Closing Graph"
    plt.close()

#
# if __name__ == '__main__':
#     x = [1,2,3,4,5,6,7,8,9,10]
#     y = [1,9,6,7,3,5,6,8,9,10]
#     plt.plot(x,y)
#     get_Screenshot("81f_0.5_amp_17k")