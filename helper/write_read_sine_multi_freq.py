from scipy.io.wavfile import read
from scipy.io.wavfile import write

import numpy as np
import matplotlib.pyplot as plt
from helper.plot_fft import plot_fft

file_name="../FSK_Baseband/sinewave_multiple_freq.wav"          # path to save wave file
# # generate simple sine wave
def sine_wave(file_name):
    fs      = 44100.0
    fc      = 18000
    ts      = 1.0/fs
    A       = 0.2
    N       = 3*fs                              # 3 sec length signal generated number of samples = 1 sec signal (generating 44100 samples)
    t       = np.arange(0,N*ts,ts)

    # sine wave for multiple frequency and test with different phase
    # sine_wave = A*np.sin(2*np.pi*17000*t)+A*np.sin(2*np.pi*(17400)*t)+A*np.sin(2*np.pi*17800*t)+A*np.sin(2*np.pi*(18200)*t)+A*np.sin(2*np.pi*(18600)*t)
    # sine_wave =A*np.sin(2*np.pi*17800*t)+A*np.sin(2*np.pi*(18200)*t)
    # sine_wave =A*np.sin(2*np.pi*1600*t)+A*np.sin(2*np.pi*(1800)*t)
    # sine_wave = A*np.sin(2*np.pi*110*t)+A*np.sin(2*np.pi*fc*t+(np.pi/4))+A*np.sin(2*np.pi*(fc*3)*t)+A*np.sin(2*np.pi*(fc*4)*t+(np.pi/2))+A*np.sin(2*np.pi*(fc*5)*t)+A*np.sin(2*np.pi*(1660)*t+(3*np.pi/4))+A*np.sin(2*np.pi*(2200)*t)

    sine_wave = A*np.sin(2*np.pi*200*t)+A*np.sin(2*np.pi*(600)*t)+A*np.sin(2*np.pi*1000*t)+A*np.sin(2*np.pi*(1600)*t)+A*np.sin(2*np.pi*(1800)*t)+A*np.sin(2*np.pi*(2000)*t)
    plt.subplot(2,1,1)
    plt.plot(t,sine_wave)
    plt.title("input signal")
    plt.subplot(2,1,2)
    plot_fft(sine_wave,fs,'r')
    plt.title("FFT plot of sine wave")
    plt.show()

    ## write signal to . wav
    sine_read_float = np.array(sine_wave, dtype=np.float)
    write(file_name, fs, sine_read_float)
    return sine_wave


#  code to read signal and plot its fft
sine_multi_freq=read(file_name)[1]
plot_fft(sine_wave,44100,'r')
plt.title("FFT plot of sine wave")
plt.show()



