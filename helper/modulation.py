import scipy.io.wavfile as sci
import numpy as np
import os, json
from helper.rrc_test_wrt_matlab import RRC
import matplotlib.pyplot as plt
from plot_fft import plot_fft
from scipy.io.wavfile import read


BASE_DIRCTORY   = os.path.dirname(os.path.abspath(__file__)).split("helper")[0]
configration    = os.path.join(BASE_DIRCTORY,'mainconfig_upfirdn.json')




class Modulation():
    # print "rrc filter activated"
    def __init__(self, **kwargs):
        self.algo               = kwargs["algo"]
        self.type_signal        = kwargs["type_signal"]
        self.data_bits          = kwargs["data_bits"]
        self.fc                 = kwargs["fc"]



        with open(configration) as data_file:

            config = json.load(data_file)['configrations'][self.algo]

        self.ov_fact         = config["ov_fact"]
        self.span            = config["span"]
        self.A               = config["amp"]

    @property
    def mod(self):
        data_repeat_in_final = []
        data_repeat_quard_final = []

        ######### basic convolution and convoultion using upsampling and filtering
        self.rrc_op = RRC(algo=self.type_signal,ov_fact=self.ov_fact).get_rrc_factor  # impulse response of  rrc pulse shaping filter



        if self.algo=="sync" or self.algo=="data_bpsk":
            print self.ov_fact
            data_up = self.upsample(self.data_bits, self.ov_fact)  # 161 frames # 4 symbol per rrc
            # print "data_up",len(data_up)
            conv_op = np.convolve(self.rrc_op, data_up, "full")
            # print "conv",len(conv_op)
            conv_op = conv_op[(self.span / 2) * self.ov_fact:-(self.span / 2) * self.ov_fact]  #
            # print "conv_ sample removed both side",len(conv_op)
            mod_op = self.carrier_mod_bpsk(conv_op, self.fc,self.A)
            print "after mod",len(mod_op)

            print "\n\n"



        else:
            print self.ov_fact
            ###   1.  extracting each bit as odd and even to divide to inphase and quadrature phase  2. basic convolution and convolution using upsampling and filtering
            inphase_bits = self.data_bits[::2]  # in phase / odd bits
            quardphase_bits = self.data_bits[1::2]  # quard phase / even bits

            inphase_data_up = self.upsample(inphase_bits, self.ov_fact)  # upsampling to 2*Tb= Tsys
            # print "inphase data_up", len(inphase_data_up)
            data_repeat_in_final = np.append(data_repeat_in_final, inphase_data_up)
            inphase_conv_op = np.convolve(self.rrc_op, inphase_data_up,"full")
            # print "inphase conv_op", len(inphase_conv_op)
            inphase_conv_op = inphase_conv_op[(self.span / 2) * self.ov_fact:-(self.span / 2) * self.ov_fact]  #
            # print "inphase conv_op_resized", len(inphase_conv_op)

            inphase_mod_op = self.carrier_mod_qpsk(inphase_conv_op, self.fc,self.A, type="inphase")
############## quadratre path modulation
            quardphase_data_up = self.upsample(quardphase_bits,self.ov_fact)  # upsampling to 2*Tb= Tsys
            data_repeat_quard_final = np.append(data_repeat_quard_final, quardphase_data_up)
            quardphase_conv_op = np.convolve(self.rrc_op, quardphase_data_up,"full")
            quardphase_conv_op = quardphase_conv_op[(self.span / 2) * self.ov_fact:-(self.span / 2) * self.ov_fact]  #
            quardphase_mod_op = self.carrier_mod_qpsk(quardphase_conv_op, self.fc,self.A, type="quardphase")


            mod_op = inphase_mod_op - quardphase_mod_op

            conv_op = inphase_conv_op - quardphase_conv_op  # for barker peak detection during baseband convolution

            # plt.subplot(3,1,1)
            # plt.plot(inphase_conv_op)
            # plt.subplot(3,1,2)
            # plt.plot(quardphase_conv_op)
            # plt.subplot(3,1,3)
            # plt.plot(conv_op)
            # plt.show()
        print "length of mod op", len(mod_op)


        return  mod_op,conv_op




    # function to perform upsampling operation zero padding before 1st sample and between 2 samples
    # example : upsampling_factor=4 data=[1,-1,1] data_upsampled=[0,0,0,1,0,0,0,-1,0,0,0,1,0,0,0]

    def upsample(self,arr, up_sample_factor):

        le = len(arr)
        y = np.zeros((up_sample_factor-1,le))  # N-1 zeros inserted between two samples
        c = np.vstack((y, arr))                # concatination vertically
        final = c.transpose()
        n = final.size
        array_up = np.reshape(final, n)
        c = np.concatenate((array_up, np.zeros(up_sample_factor-1)))
        # c=c[up_sample_factor/2:]
        return c

    def carrier_mod_bpsk(self,conv_op,fc,A):  # at modulator end data after upsampling multipled by sin( 2*pi*fc*t)
        print "activated sync or bpsk data modulation"
        fs=44100
        ts = 1.0 / float(fs)
    ############full modulation
        frames=len(conv_op) # complete data modulation
        t2 = np.arange(0, (frames - ts) * ts, ts)
        carr_signal = A * np.sin(2 * np.pi * fc * t2)
        modulated_signal = conv_op * carr_signal
        return modulated_signal

    def carrier_mod_qpsk(self,conv_op,fc,A,type):  # at modulator end data after upsampling multipled by sin( 2*pi*fc*t)
        print "activated qpsk data modulation for inphase and quard phase paths"
        fs=44100
        ts = 1.0 / float(fs)
        frames=len(conv_op) # complete data modulation

        t2 = np.arange(0, (frames - ts) * ts, ts)

        if type == "inphase":
            carr_signal = A * np.cos(2 * np.pi * fc * t2)
        else:
            carr_signal = A * np.sin(2 * np.pi * fc * t2)

        modulated_signal = conv_op * carr_signal

        return modulated_signal



