import numpy as np

def convert_float_array_to_int(array, precission):
    '''
    converting float array to int with precission.
    [0.2, 0.05, 0.7] with precission 2 = [20,5,70]
    :param array: float array
    :param precission: default 5
    :return: int array converted
    '''
    rounded_arr = np.round(array, precission)
    multiply = pow(10, precission)
    mutiplied_arr = np.multiply(rounded_arr, multiply)
    converted_arr = np.array(mutiplied_arr, np.int16)
    return converted_arr