import xlrd
import json

def readData(workbook):
    workbook = xlrd.open_workbook(workbook)
    worksheet = workbook.sheet_by_index(0)

    data = []
    keys = [v.value for v in worksheet.row(0)]
    for row_number in range(worksheet.nrows):
        if row_number == 0:
            continue
        row_data = dict()
        for col_number, cell in enumerate(worksheet.row(row_number)):
            row_data[keys[col_number]] = cell.value
        data.append(row_data)

    return data

if __name__ == '__main__':
    data_json=readData("/home/roopa/qpsk_milica/plot_tra_per_data_error_per_room.xlsx")
    with open("error signal room.json", 'w') as outfile:
        json.dump(data_json, outfile, sort_keys=True, indent=4, ensure_ascii=False)
