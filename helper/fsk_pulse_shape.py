import numpy as np
import os, json
import matplotlib.pyplot as plt


BASE_DIRCTORY   = os.path.dirname(os.path.abspath(__file__)).split("helper")[0]
configration    = os.path.join(BASE_DIRCTORY,'mainconfig_fsk.json')

class PulseShaping():


    def __init__(self, **kwargs):
        self.algo_type              =   kwargs.get("algo")



        with open(configration) as data_file:
            config = json.load(data_file)["configrations"][self.algo_type]


        # getting configration from config file
        self.Fs             = config["fs"]
        self.ov_fact        = config["ov_fact"]
        self.span           = config ["span"]
        self.fc             = config ["fc"]
        if self.algo_type=="gaussian" or self.algo_type  == "gaussian_mfsk" :
            self.roll_off       = config["roll_off"]
            self.no_of_zeros    = config["no_of_zero_padding"]
            self.samples_per_symbol  = (self.ov_fact*self.span) - self.no_of_zeros
        elif self.algo_type  == "RRC" or self.algo_type  == "sync":
            self.roll_off       = config["roll_off"]
            self.samples_per_symbol  = (self.ov_fact*self.span)
        elif self.algo_type=="Eye_shape" or self.algo_type=="Eye_shape_smooth_end":
            self.damping_per=config["damping_perc"]
            self.amp=config["amp"]

    def pad_zeros(self,data, no_of_zeros):
        zero_pad = np.zeros(no_of_zeros)
        gauss_resp_zero_pad = np.concatenate([zero_pad, data, zero_pad])
        return gauss_resp_zero_pad


    @property
    def getpulseshape(self):

        if self.algo_type  == "gaussian" or self.algo_type  == "gaussian_mfsk":
            print "activated Gaussian pulse shape with frames", self.samples_per_symbol
            Tb         = float(self.samples_per_symbol) / float(self.Fs)
            roll_of    = Tb*self.roll_off
            t = np.transpose(np.linspace(-self.span * Tb / 2, self.span * Tb / 2, self.ov_fact))

            # gaussian equation. generates gaussian response
            gauss_resp = np.sqrt(np.pi) / roll_of * np.exp(-(np.pi * t / roll_of) ** 2)

            # normalization of gaussian result to unit amp.
            gauss_resp1 = gauss_resp / float(np.max(gauss_resp))

            # adding zero on each side of gaussian response.
            gauss_resp_final =self. pad_zeros(
                data=gauss_resp1, no_of_zeros=self.no_of_zeros
            )
            print "length of gaussian filter",len(gauss_resp_final)
            return gauss_resp_final



        elif self.algo_type  == "RRC" or self.algo_type  == "sync" :
            # print "pulse shaping for sync","\n \n"
            step = 1 / float(self.ov_fact)
            ######### order of filter should be even
            if (self.span * self.ov_fact) % 2 != 0.0 or self.roll_off > 1.0:
                order = self.span* self.ov_fact
                raise Exception("filter order is not even or roll off not valid")

            delay = (self.span * self.ov_fact) / 2.0
            Ts = float(1) / float(self.Fs)

            t = np.arange((-delay * step), (delay * step) + step, step)

            p = np.zeros_like(t)

            # implementation of Squareroot raisercoisne  for equation implementation : https://en.wikipedia.org/wiki/Root-raised-cosine_filter
            for i in range(0, len(t)):
                if t[i] == 0:
                    p[i] = 1.0 / np.sqrt(Ts) * ((1 - self.roll_off) + 4 * self.roll_off / np.pi)
                elif t[i] == float(1) / float(4 * self.roll_off) or t[i] == -float(1) / float(4 *self.roll_off):
                    p[i] = self.roll_off / np.sqrt(2 * Ts) * ((1 + 2 / np.pi) * np.sin(np.pi / (4 * self.roll_off)) + (1 - 2 / np.pi) * np.cos(np.pi / (4 *self.roll_off)))
                else:
                    # print "t val",t[i]
                    Numerator = np.float(np.sin(np.pi * t[i] * (1 - self.roll_off)) + 4 * self.roll_off * t[i] * np.cos(np.pi * t[i] * (1 + self.roll_off)))
                    # print "numerator", Numerator
                    dinominator = np.float(np.pi * t[i] * (1 - (4 * self.roll_off * t[i]) ** 2))
                    # print "dinominator", dinominator
                    # if dinominator==0:
                    #     dinominator=np.finfo(float).eps
                    #     print "+++++++++++++++++++>>>>>>>>>>>" , 1 / np.sqrt(Ts) * Numerator / dinominator
                    # else:
                    p[i] = 1 / np.sqrt(Ts) * Numerator / dinominator

            # Normalization to unit energy rms
            rrcfilt = p / float(np.sqrt(np.sum(p ** 2)))

            return rrcfilt

        elif self.algo_type  =="Eye_shape_fc":

                print "pulse shaping - Eye shape"

                A = 0.07  # amplitude for -26dB power

                ts = float(1) / float(self.Fs)

                t2 = np.arange(0, (self.ov_fact - ts) * ts, ts)

                t = np.linspace(0, A, (0.3 * self.ov_fact))  # 30% of size

                A0 = (A * np.exp(-100 * t))

                A0_rev = A0[::-1]

                damp_point_1 = len(A0)

                damp_point_2 = self.ov_fact - len(A0)

                # print len(A0), len(A0_rev), size, Silence_GAP

                sine_signal_wave = A * np.sin(2 * np.pi * self.fc * t2[damp_point_1:damp_point_2])

                sine_damp_down = A0 * np.sin(2 * np.pi * self.fc * t2[damp_point_2:])

                sine_damp_up = A0_rev * np.sin(2 * np.pi * self.fc * t2[0:damp_point_1])

                sine_signal_test_1 = np.append(sine_damp_up, sine_signal_wave)

                sine_signal = np.append(sine_signal_test_1, sine_damp_down)

                # print "damp up", len(sine_damp_up),"damp down", len(sine_damp_down)
                # plt.plot(sine_signal)
                # plt.title("eye shape")
                # plt.show()
                # # raw_input()

                return sine_signal


        elif self.algo_type == "Eye_shape":
            print "pulse shaping for data : eye shape with %d dampling perc"% self.damping_per ,"\n \n"
        ## amplitude increase and decrease length
            amp_increase = int(np.round(self.damping_per * self.ov_fact))
            amp_constant = self.ov_fact
            linear_raise_array = []
            constant_amp = []
            l = np.linspace(0, 1, amp_increase)
            # l=np.arange(0,amp_increase*ts,ts)
            for i in range(0, amp_constant + amp_increase):
                if i < len(l):
                    # print i
                    linear_raise = self.amp * l[i]
                    linear_raise_array = np.append(linear_raise_array, linear_raise)
                elif i >= len(l):
                    # print i
                    constant_amp = np.append(constant_amp, self.amp)
            linear_fall_array = linear_raise_array[::-1]
            linear_pulse_shape_array = np.append(linear_raise_array, constant_amp)
            linear_pulse_shape_array = np.append(linear_pulse_shape_array, linear_fall_array)



            return linear_pulse_shape_array

        elif self.algo_type == "Eye_shape_smooth_end":
                ## amplitude increase and decrease length
            amp_increase = int(np.round(self.damping_per * self.ov_fact))
            amp_constant = self.ov_fact
            linear_raise_array = []
            constant_amp = []
            l = np.linspace(0, 1, amp_increase)
            l = l[::-1]
            print l, "\n"
            for i in range(0, amp_constant+amp_increase):
                if i < len(l):
                    linear_raise = self.amp * np.cos((np.pi / 2) * l[i])
                    print linear_raise

                    linear_raise_array = np.append(linear_raise_array, linear_raise)
                    # print "end_1"
                elif i >= len(l):
                    constant_amp = np.append(constant_amp, self.amp)

            linear_fall_array = linear_raise_array[::-1]
            # print linear_fall_array, linear_raise_array
            linear_pulse_shape_array = np.append(linear_raise_array, constant_amp)
            linear_pulse_shape_array = np.append(linear_pulse_shape_array, linear_fall_array)
            # plt.plot(linear_pulse_shape_array)
            # plt.show()
            # raw_input()
            return linear_pulse_shape_array




