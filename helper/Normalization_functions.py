
import numpy as np
import matplotlib.pyplot as plt

def agc(received_signal,required_gain):

    # to find gain of input signal to adjust
    print "agc activated"
    length_of_signal=len(received_signal)
    # defining input threshold below which signal will be noise need not be enhances
    Threshold=-30    #  received signal should be alteast == threshold  in our case -6 dB is our received signal to be enhanced to 0 dB and should be inaudible

    # output power should be in the range
    # min_power = -10
    # max_power = -20     #Humans can hear sounds between 0 and 140 decibels.  0 decibel does not mean that there is no sound, merely that we cannot hear it. 0 decibel is the so-called hearing threshold for the human ear.


    # calculating logarithmic power of input  ----> p (in db) = 10*log10(p/p_ref)    since the value of p_ref is taken as 1 Watt , equation becomes    p (in db) = 10*log10(p)
    # p=sum(received_signal(i).^2)/N

    input_power_db=10*np.log10(np.sum(received_signal**2)/length_of_signal)
    print "power of input signal is %.2f  dB"%input_power_db
   #check for threshold
    if input_power_db < Threshold:
        print "input thershold id below %d threshold db"%Threshold
    # to check if input gain level is less or more then level specified
    # if (required_gain < min_power) or (required_gain > max_power):
    #     print "required gain is out of given limit %d min_power %d max_ power"%(min_power,max_power)

# from required gain level calculate power need to be adjusted
# formula : gain in db=10 log10(pout/pin) : power ratio
        #  gain in db= 20 log10(vout/vin): amplitude ratio
    output_power_required=10**(required_gain/10)
    ## calculating gain adjustmen required using formula K1 = sqrt( (output_power_required*N) / input_energy)
    input_energy=np.sum(received_signal**2)
    # gain_constant=k1
    k1 = np.sqrt((output_power_required*length_of_signal)/input_energy)
    output_enhance_signal=k1*np.array(received_signal)
    output_power_db = 10 * np.log10(np.sum(output_enhance_signal ** 2)/length_of_signal)
    print "power of outputsignal is %.2f dB" % output_power_db
    # final_gain=10 * np.log10(output_power_db/input_power_db)
    # print "gain in dB",final_gain


    return output_enhance_signal

def Normalization (data,type):
    '''type: acg= automatic gain control
             rms = root mean square
             divide by peak amplitude '''

    if type=="agc":

        # AUTOMATIC GAIN CONTROL loop to  adjust input power to zero db output power  data normalization using case 1: Automatic gain control
        #GAIN NEEDED IN OUTPUT SIGNAL OF AGC  http://www.sengpielaudio.com/calculator-levelchange.htm

        required_gain =10  # power gain increased by 10
        normalized_data = agc(data, required_gain)
    elif type=="rms":
        # normalization using RMS [root mean square]  http://in.mathworks.com/help/signal/ref/rms.html

        normalized_data = data/ float(np.sqrt(np.mean(data ** 2)))                      # normalize RMS


    ##data normalization using case 1:divide max peak amplitude

    else:
        normalized_data= data/ float(np.max((data)))                                  # normalize max peak value


    # plt.suptitle("before and after normalizing")
    # plt.subplot(2,1,1)
    # plt.plot(data[:600])
    # plt.title("before normalization")
    # plt.subplot(2,1,2)
    # plt.plot(normalized_data[:600])
    # plt.title("after normalization by %s type"%type)
    # plt.show()

    return normalized_data
