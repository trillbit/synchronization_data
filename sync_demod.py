'''Introduction :  call function:  main_sync_demodulation(received signal , peak_logic="1_peak" or "2_peak")  '''

from scipy.io.wavfile import read
from src.helper.filternew import bpf_filtfilt
from src.helper.fsk_pulse_shape import PulseShaping
import matplotlib.pyplot as plt
import numpy as np
import json
import time
import os

t_time = time.time()

BASE_DIRECTORY   = os.path.dirname(os.path.abspath(__file__)) + "/../.."
CONFIG_DIRECTORY = os.path.join(BASE_DIRECTORY, "src/config")

configuration = os.path.join(CONFIG_DIRECTORY, "mainconfig.json")



def top_2peaks( seq, min_dis_btw_peaks, no_peaks):

    sorted_seq = np.argsort(seq)[-no_peaks:]  # consider top 6 peaks according to increasing order
    top_peaks = sorted_seq[::-1]
    if (peak_logic == "2_peak"):
        count = 0
        for i in range(0, no_peaks - 1):
            diff = np.abs(top_peaks[i + 1] - top_peaks[0])

            if diff >= min_dis_btw_peaks:
                count = count + 1
                break

        return top_peaks[0], top_peaks[i + 1]
    else:
        return top_peaks[0]


def peak_detection(mag, window_size):
    inver_mag = mag[::-1]
    ref_avg = np.average(inver_mag[:3 * window_size])
    ref_ratio = ref_avg / 8.0
    ref_count = 0
    ref_index = 0
    for i in range(3 * window_size, len(mag), window_size):
        if ref_ratio > np.average(inver_mag[i:i + window_size]):
            ref_count += 1
            ref_index = i
            if ref_ratio > np.average(
                    inver_mag[i + window_size:i + 2 * window_size]):  # 2nd check if ve interend to noise floor
                break
            break
    # print "ref index", len(mag)-ref_index

    sorted_seq = np.argmax(mag[:len(mag) - ref_index])  # consider top 6 peaks according to increasing order
    top_peaks = mag[sorted_seq]

    print "index", sorted_seq, "val at index", top_peaks

    return sorted_seq


def carrier_demod( conv_op,fc_sync, A_sync,fs, phi):

    ts = 1.0 / float(fs)
    # ###########full modulation
    t2 = np.arange(0, (len(conv_op) - ts) * ts, ts)
    real_signal = A_sync * np.sin(2 * np.pi * fc_sync  * t2 + phi)
    imag_signal = A_sync * np.cos(2 * np.pi * fc_sync  * t2 + phi)
    carr_signal = []
    for i in range(0, len(real_signal)):
        complex_signal = np.complex(real_signal[i], imag_signal[i])
        carr_signal = np.append(carr_signal, complex_signal)
    modulated_signal = conv_op * carr_signal
    return modulated_signal



def main_sync_demodulation(received_signal,peak_logic):
    ''' received_signal: .wav file with has barker and modulated data
             peak_logic:"1_peak" in barker or  "2_peak" in barker '''


    # reading from configration file
    with open(configuration) as data_file:
        config = json.load(data_file)

    # getting sync and other configrations
    default_sync_case = config["default"]["sync"]  ############# change this if number of frames varied
    size_of_data = config["default"]["length_of_data"]
    sync_configration = config["configrations"][default_sync_case]
    dis_btwn_p2_data_1 = sync_configration['dis_btween_p2_data_1']
    samples_per_symbol = sync_configration["ov_fact"]
    span = sync_configration["span"]
    fc_sync = sync_configration["fc"]
    fs = sync_configration["fs"]
    A_sync = sync_configration["amp"]
    sync_sample = config["default"]["sync_sample"]

    sync_baseband = np.load("../Fsk_mod_demod/recording/sync_file.npy")
    data = read(received_signal)[1]           # read data file
    data_bpf = bpf_filtfilt(signal_to_filter=data, lower_cutoff=16000, higher_cutoff=19000, sampling_rate=44100,
                            transition_width=105, ripple_db=120)


   ### sync correlation--> peak detection data Extraction, RRC impulse response ---> demodulation (complex) ---> matched filter
    data_corr = data_bpf[:sync_sample]
    h = PulseShaping(algo="sync").getpulseshape
    demod_op = carrier_demod(data_corr, fc_sync, A_sync,fs, phi=0)
    matched_filt = np.convolve(h, demod_op, "full")
    matched_filt = matched_filt[(span / 2) * samples_per_symbol:-(span / 2) * samples_per_symbol]
    barker_corr_real = np.correlate(np.real(matched_filt), sync_baseband, "same")
    barker_corr_img = np.correlate(np.imag(matched_filt), sync_baseband, "same")
    mag = np.sqrt(np.add(np.square(barker_corr_real), np.square(barker_corr_img)))


    ## get top 2 peaks min distance  used in case of 2 barker
    # min_dis_btw_peaks = 100
    # if (peak_logic == "2_peak"):
    #     peaks1, peaks2 =top_2peaks(mag, min_dis_btw_peaks,no_peaks=40)  # actual top 20 peaks with 2665 as min distance
    #
    #     peakmax_2 = max(peaks1, peaks2)
    #
    # else:
    #     peakmax_2 = top_2peaks(mag, min_dis_btw_peaks, no_peaks=40)


    ## peak detetcion based on noise floor
    window_size = 850                                                                       ## sync is 0.19 sec divided to 10 chunks of 850 samples
    peakmax_2 = peak_detection(mag, window_size)

    data_length = (size_of_data * samples_per_symbol)
    data_start = peakmax_2 + dis_btwn_p2_data_1
    data_end = int(data_start + data_length)
    data_frame_extracted = data_bpf[data_start:data_end]
    len_each_bit = len(data_frame_extracted) / float(size_of_data)

    elapsed_time = time.time() - t_time
    print 'elapsed time for demodulation', elapsed_time

    return data_frame_extracted


if __name__ == '__main__':
    type = "MT_FSK"
    file = "../synchronization_data/wave_file/default_0.wav/default_0.wav" # synthetic signal
    distance = "unrec"
    peak_logic = "1_peak"
    main_sync_demodulation(received_signal=file,peak_logic=peak_logic)

