# introduction : callfunction:  final_data=main_sync_modulation( Modulated_data) pass modulated data to append it with barker synch start
            #Modulated_data array which uyou want to concatenate -QPSK/BPSK/Chirp Case..
            # final_data= appended b arker and modulated data --> u have to save this array in .wav format in calling function



import numpy as np
import matplotlib.pyplot as plt
from helper.plot_fft import plot_fft

#  read numpy array as parameters not changing array will be same

# ############ code 2
# def main_sync_modulation(Modulated_data):
#     sync_start=np.load('../synchronization_data/mod_passband_sync_1_barker.npy')      # 1 barker
# #     sync_start=np.load('../synchronization_data/mod_passband_sync_2_barker.npy')        # 2 barker
#     final_data = np.append(sync_start, Modulated_data)
#     plt.plot(sync_start)
#     plt.show()
#
#
#     return final_data
#
# if __name__ == '__main__':
#     Modulated_data=[1,1,-1,1]
#     main_sync_modulation(Modulated_data)

#

# case 1: read barker bits--> upsample--> modulate
import os,json

from helper.rrc_test_wrt_matlab import RRC
BASE_DIRCTORY = os.path.dirname(os.path.realpath(__file__))
configration = os.path.join(BASE_DIRCTORY, 'mainconfig_fsk.json')
#Globals
sync_bits = [ 1, 1, 1, 1, 1, -1, -1, 1, 1, -1, 1, -1, 1]


with open(configration) as data_file:
    config = json.load(data_file)
    empty_zeros = config["default"]["empty_zeros"]
    data_configration = config["configrations"]["sync"]
    fs = data_configration["fs"]
    A = data_configration["amp"]
    ov_fact = data_configration["ov_fact"]
    span = data_configration["span"]
    fc = data_configration["fc"]
    print fs,fc,A,ov_fact,span,empty_zeros


def upsample(arr, up_sample_factor):
    le = len(arr)
    y = np.zeros((up_sample_factor - 1, le))  # N-1 zeros inserted between two samples
    c = np.vstack((y, arr))  # concatination vertically
    final = c.transpose()
    n = final.size
    array_up = np.reshape(final, n)
    c = np.concatenate((array_up, np.zeros(up_sample_factor - 1)))
    return c



def carrier_mod_bpsk(conv_op, fc, A,fs):  # at modulator end data after upsampling multipled by sin( 2*pi*fc*t)
    print "activated sync modulation with amp", A
    ts = 1.0 / float(fs)
    frames = len(conv_op)  # complete data modulation
    t2 = np.arange(0, (frames - ts) * ts, ts)
    carr_signal = A * np.sin(2 * np.pi * fc * t2)
    modulated_signal = conv_op * carr_signal

    return modulated_signal

def mod(data_bits):

        # pulse_resp = PulseShaping(algo="sync").getpulseshape
        pulse_resp = RRC(algo="sync").get_rrc_factor
        pulse_resp = pulse_resp / max(pulse_resp)

        data_up = upsample(data_bits, ov_fact)
        conv_op = np.convolve(pulse_resp, data_up, "full")
        conv_op = conv_op[(span / 2) * ov_fact:-(span / 2) * ov_fact]  #
        mod_op = carrier_mod_bpsk(conv_op, fc, A,fs)
        return mod_op

def main_sync_modulation(Modulated_data,No_barker_Modulation):

    ''' No_barker_Modulation= 1 barker or 2 barker '''

    sync_empty = np.zeros((1,empty_zeros))            # number of zeros should be multiple of ov_fact
    # --For 1 barker
    if No_barker_Modulation==1:
        frame = np.append(sync_empty, sync_bits)
        frame_start = np.append(frame, sync_empty)
    else:
    # --For 2 barker
        frame = np.append(sync_empty, sync_bits)
        frame1 = np.append(frame, sync_empty)
        frame2 = np.append(frame1, sync_bits)
        frame_start = np.append(frame2, sync_empty)

    mod_passband_sync = mod(frame_start)
    np.save("mod_passband_sync_2_barker.npy",mod_passband_sync)

    final_data = np.append(mod_passband_sync, Modulated_data)
    plt.subplot(2,1,1)
    plt.plot(mod_passband_sync)
    plt.subplot(2,1,2)
    plot_fft(mod_passband_sync,fs,'r')
    plt.title("mod fsk data")
    plt.show()


    # from helper.convert_float_array_to_int import convert_float_array_to_int
    # import scipy.io.wavfile as sci
    # qpsk_complete = np.array(mod_passband_sync, dtype=np.float)
    # flot_int = convert_float_array_to_int(qpsk_complete, 4)  # precision 5 dafault
    # sci.write("sync_audiblity", 44100, flot_int)

    return final_data



if __name__ == '__main__':
    Modulated_data=[1,1,-1,1]
    main_sync_modulation(Modulated_data,No_barker_Modulation=1)



